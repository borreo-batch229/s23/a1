//console.log("Hello World!")

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		Hoenn: ["May", "Max"],
		Kanto: ["Brock", "Misty"]
		},
	talk: function(){
		console.log('Pikachu! I choose you!');
	}
};
console.log(trainer);
console.log(trainer.name);
console.log(trainer["pokemon"]);
trainer.talk();

function pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;

	this.tackle = function(targetPokemon){
	console.log(this.name + " tackled " + targetPokemon.name)
	}
	
	this.faint = function(){
		console.log(this.name + " has fainted!")
	}
	
}

let newPokemon1 = new pokemon("Pikachu", 12);
console.log(newPokemon1);

let newPokemon2 = new pokemon("Geodude", 8);
console.log(newPokemon2);

let newPokemon3 = new pokemon("Mewtwo", 100);
console.log(newPokemon3);

 newPokemon2.tackle(newPokemon1)
 newPokemon3.tackle(newPokemon2)
 newPokemon3.faint()

